$(document).ready(function(){
	// list-box
	$('.list-box-toggle .simple-list-toggle').hide()
	$('.list-box-toggle .gray-box').click(function(){
		$(this).parent().find('.simple-list-toggle').toggle();
	});

	// simple-list
	$('.simple-list-toggle ul').hide();
	$('.simple-list-toggle a').click(function(e){
		var simpleListUl = $(this).next('ul');
		if( (simpleListUl.is('ul')) ) {
			e.preventDefault();
			simpleListUl.slideToggle();
		}
	});

	// action
	$('.action .border-box').hide()
	$('.action h4').click(function(e){
		e.preventDefault();
		$(this).parent().find('.border-box').toggle();
	});	

	// fabcybox
	$('.fancybox').fancybox();

	// flexslider
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 147,
		itemMargin: 0,
		asNavFor: '#slider'
	});
	$('#slider').flexslider({
		animation: "fade",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
		start: function(slider){
			$('body').removeClass('loading');
		}
	});
});
