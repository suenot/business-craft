#!/usr/bin/env python

from flask import Flask
from flask import render_template
from jinja2.exceptions import TemplateNotFound
app = Flask(__name__)
app.config.update(
    SEND_FILE_MAX_AGE_DEFAULT = 0
)
app.jinja_env.add_extension('jinja2htmlcompress.SelectiveHTMLCompress')
import mimetypes
mimetypes.add_type('image/svg+xml', '.svg')
mimetypes.add_type('image/png', '.png')
@app.route('/')
@app.route('/<page>/')
def any_page(page='index'):
    try:
        return render_template(page + '.html')
    except TemplateNotFound:
        return render_template('404.html')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)